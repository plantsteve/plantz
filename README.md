Our friendly plant specialist will come into your office and enhance the space with beautiful green plants, plant walls, and other enhancements to make your space come to life. Our specialist will then ensure those plants are well maintained on an ongoing basis for a worry free experience.

Address: 7557 W Sand Lake Rd, #1014, Orlando, FL 32819

Phone: 407-442-0304